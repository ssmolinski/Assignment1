// Assignment1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <map>
#include <bitset>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

class HexConverter
{
private:
	map<char, bitset<8>> hexValues;
	int numberOfBytes;
	bitset<8>* byteArray;

private:
	bitset<8> getHexValue(char value);
	bitset<8> getByte(char highBits, char lowBits);
	vector<char> filterFile(string filePath);
	void createByteArray(vector<char> charsFromFile);
	void sortBytes(vector<bitset<8>> &hexValues);
	bool compareBytes(bitset<8> byte1, bitset<8> byte2);

public:
	void createOutputFile(string inputFilePath);
	HexConverter();
};

HexConverter::HexConverter()
{
	char hexValue = '0';

	for (int i = 0; i < 16; i++)
	{
		if (hexValue > '9' && hexValue < 'A')
			hexValue = 'A';

		hexValues.insert(pair<char, bitset<8>>(hexValue, std::bitset<8>(i)));
		hexValue++;
	}
}

bitset<8> HexConverter::getHexValue(char value)
{
	auto it = hexValues.find(value);
	if (it == hexValues.end())
	{
		throw invalid_argument(string(1, value) + " is not a valid hex value.\n");
	}

	return it->second;
}

bitset<8> HexConverter::getByte(char highBits, char lowBits)
{
	bitset<8> high = getHexValue(highBits) << 4;
	bitset<8> low = getHexValue(lowBits);

	return (high | low);
}

vector<char> HexConverter::filterFile(string filePath)
{
	ifstream fileIn;
	vector<char> charsFromFile;

	fileIn.open(filePath);
	if (fileIn.is_open())
	{
		char c;
		while (fileIn.get(c))
		{
			if (c >= 'A' && c <= 'F' || c >= '0' && c <= '9')
			{
				charsFromFile.push_back(c);
			}
		}
	}	

	return charsFromFile;
}

void HexConverter::createByteArray(vector<char> charsFromFile)
{
	int numberOfChars = charsFromFile.size();

	if (numberOfChars % 2 != 0)
	{
		numberOfBytes = numberOfChars / 2 + 1;
		byteArray = new bitset<8>[numberOfBytes];
		byteArray[numberOfBytes - 1] = getByte('0', charsFromFile.at(numberOfBytes - 1));
	}
	else
	{
		numberOfBytes = numberOfChars / 2;
		byteArray = new bitset<8>[numberOfBytes];
	}
	

	for (int i = 0, j = 0; i < numberOfChars /2; i++, j += 2)
	{
		byteArray[i] = getByte(charsFromFile.at(j), charsFromFile.at(j + 1));
	}
}

bool HexConverter::compareBytes(bitset<8> byte1, bitset<8> byte2)
{
	return byte1.to_ulong() > byte2.to_ulong();

}

void HexConverter::sortBytes(vector<bitset<8>> &hexValues)
{
	sort(hexValues.begin(), hexValues.end(), [](const auto & lhs, const auto & rhs)
	{ return lhs.to_ulong() < rhs.to_ulong(); });
}


void HexConverter::createOutputFile(string inputFilePath)
{
	vector<char> charsFromFile = filterFile(inputFilePath);
	createByteArray(charsFromFile);

	vector<bitset<8>> evenOnesValues;
	vector<bitset<8>> oddOnesValues;

	for (int i = 0; i < numberOfBytes; i++)
	{
		if (byteArray[i].count() % 2 == 0)
			evenOnesValues.push_back(byteArray[i]);
		else
			oddOnesValues.push_back(byteArray[i]);		
	}
	sortBytes(evenOnesValues);
	sortBytes(oddOnesValues);

	ofstream outFile;

	outFile.open("out.txt");
	if (outFile.is_open())
	{
		for (auto it : evenOnesValues)
			outFile << setfill('0') << std::setw(2) << hex << uppercase << it.to_ulong() << endl;

		outFile << endl;

		for (auto it = oddOnesValues.rbegin(); it != oddOnesValues.rend(); ++it)
			outFile << setfill('0') << std::setw(2) << hex << uppercase << it->to_ulong() << endl;
	}

	delete [] byteArray;

}

int main()
{
	HexConverter converter;
	string filePath = ".\\znaki.txt";
	converter.createOutputFile(filePath);

	
	return 0;
}
